import { Amplify } from 'aws-amplify';
import { Auth } from 'aws-amplify';
import { Authenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import { useAuthenticator } from '@aws-amplify/ui-react';
import { useSession } from "next-auth/react"

import awsExports from '../src/aws-exports';
Amplify.configure({ ...awsExports, ssr: true });

const formFields = {
  signUp: {
    name: {}
  }
}
export function AuthSession() {
  const { user, signOut } = useAuthenticator((context) => [context.user]);
  Auth.currentSession().then(data => console.log(data.getAccessToken()));
  return (
    <main>
        <h1>Hello {user?.username}</h1>
        <button onClick={signOut}>Sign out</button>
    </main>
  );
}