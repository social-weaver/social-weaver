FROM gitpod/workspace-full:latest

RUN pip3 install --upgrade pip \
    && pip3 install awscli

RUN npm install -g aws-cdk

RUN npm install -g @aws-amplify/cli

USER gitpod

RUN mkdir -p "$HOME/.zsh" \
    && git clone --depth=1 https://github.com/spaceship-prompt/spaceship-prompt.git "$HOME/.zsh/spaceship" \
    && echo 'source "$HOME/.zsh/spaceship/spaceship.zsh"' >> "$HOME/.zshrc"
